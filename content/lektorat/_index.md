---
title: "RehBuck Lektorat"
motto: "Texte mit Stil"
date: 2018-01-22
publishdate: 2017-01-23
subheader: Sprache ist das Kleid der Gedanken.

---

Jeder Text verlangt ein passendes Outfit, von der Wortwahl über satzbau zum Stil. Sprachlich korrekter Ausdruck, Rechtschreibung, Zeichensetzung und grammatishce Sicherheit machen den Auftritt perfekt.

Sie erstellen Presse- und Werbetexte, Newsletter oder Broschüren und suchen ein professionelles Korrektorat?

Ihre geschäftliche Korrespondenz, Verträge und Gutachten verlangen eindeutige Formulierungen, fehlerfreie Grammatik und korrekte Orthographie?

Sie sind Autor und wollen Ihr Manuskript vor der Veröffentlichung begutachten und überprüfen lassen?

Sie wünschen Unterstützung bei der Formulierung deutschsprachiger Texte?

Als Lektorin korrigiere und optimiere ich Ihren Text. Ich helfe Ihnen, Ihre Gedanken klar zu formulieren und in die passende sprachliche Form zu kleiden.

Und hier noch zusätzlicher Testtext für "Lektorat".
